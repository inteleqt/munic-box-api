﻿using System;
using System.Collections.Generic;
using MD.CloudConnect;

namespace InteleqtMunicApi.Models
{
    [Serializable]
    public class MunicBox : MDData
    {
        public Meta meta { get; set; }
        public Payload payload { get; set; }
        public ITracking tracking { get; set; }
    }

    //public class Meta
    //{
    //    public string account { get; set; }
    //    public string @event { get; set; }
    //}

    //public class GPSSPEED
    //{
    //    public string b64_value { get; set; }
    //}

    //public class Fields
    //{
    //    public GPSSPEED GPS_SPEED { get; set; }
    //}

    //public class Payload
    //{
    //    public string asset { get; set; }
    //    public string channel { get; set; }
    //    public DateTime created_at { get; set; }
    //    public object parent_id { get; set; }
    //    public object parent_id_str { get; set; }
    //    public string b64_payload { get; set; }
    //    public DateTime received_at { get; set; }
    //    public string recipient { get; set; }
    //    public DateTime recorded_at { get; set; }
    //    public string sender { get; set; }
    //    public long thread_id { get; set; }
    //    public string thread_id_str { get; set; }
    //    public string type { get; set; }
    //    public object id { get; set; }
    //    public string id_str { get; set; }
    //    public List<double?> loc { get; set; }
    //    public Fields fields { get; set; }
    //}
}
