﻿using MD.CloudConnect;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace InteleqtMunicApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseMvc();
            InitializeCloudConnect();
        }

        private void InitializeCloudConnect()
        {
            //List the field that you want in the notification result
            string[] fieldsThatINeed = {
             MD.CloudConnect.FieldDefinition.GPRMC_VALID.Key,
                MD.CloudConnect.FieldDefinition.GPS_SPEED.Key,
                MD.CloudConnect.FieldDefinition.GPS_DIR.Key,
                MD.CloudConnect.FieldDefinition.ODO_FULL.Key,
                MD.CloudConnect.FieldDefinition.DIO_IGNITION.Key,
                MD.CloudConnect.EasyFleet.MDI_DRIVING_JOURNEY.Key,
                MD.CloudConnect.EasyFleet.MDI_IDLE_JOURNEY.Key,
                MD.CloudConnect.EasyFleet.MDI_JOURNEY_TIME.Key
            };

            //initialize field and object (IDataCache)
            Notification.Instance.Initialize(fieldsThatINeed, Tools.MyDataCacheRepository.Instance, true);
        }
    }
}
