﻿using System;
using MD.CloudConnect;

namespace InteleqtMunicApi.Tools
{
    public class MyDataCacheRepository : MD.CloudConnect.IDataCache
    {
        /* Singleton */
        protected static readonly MyDataCacheRepository _instance = new MyDataCacheRepository();
        public static MyDataCacheRepository Instance
        {
            get
            {
                lock (_instance)
                {
                    return _instance;
                }
            }
        }

        static MyDataCacheRepository()
        {

        }

        /* IDataCache */
        public DateTime getHistoryFor(string asset, ITracking data)
        {

            return DateTime.MinValue;
        }
    }
}
