﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using MD.CloudConnect;
using Microsoft.AspNetCore.Mvc;

namespace InteleqtMunicApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        List<ITracking> tacking = new List<ITracking>();
        // GET api/values
        [HttpGet]
        public List<ITracking> Get()
        {
            return tacking;
        }

        // POST api/values
        [HttpPost]
        public ActionResult<HttpStatusCode> Post()
        {
            string res = string.Empty;

            try
            {
                using (StreamReader stream = new StreamReader(Request.Body,Encoding.UTF8))
                {
                    res =  stream.ReadToEnd();
                }

                Decode(res);
            }
            catch (Exception error)
            {
                Console.WriteLine(error);
            }

            return HttpStatusCode.OK;
        }

        private void Decode(string data)
        {

            List<MDData> decodedData = Notification.Instance.Decode(data);
            foreach (MDData mdData in decodedData)
            {
                if (mdData.Meta.Event == "track")
                {
                    tacking.Add(mdData.Tracking);
                }
            }

            using (StreamWriter writer = new StreamWriter(@"file.txt", true))
            {
                writer.WriteLine(tacking[0].Speed);
            }
        }
    }
}
